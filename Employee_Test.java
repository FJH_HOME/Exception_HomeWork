package Exception;

import java.util.Scanner;

public class Employee_Test {
    static Scanner input=new Scanner(System.in);
    public static void main(String[] args) {
        Employee employee[]=new Employee[3];
        employee[0]=new Employee();
        employee[1]=new Employee();
        employee[2]=new Employee();
        int i=0;
        for(Employee e:employee)
        {
            i++;
            System.out.print("请输入第"+i+"名员工的工号：");
            int id=input.nextInt();
            e.setId(id);
            System.out.print("请输入第"+i+"名员工的姓名：");
            String name=input.next();
            e.setName(name);
            System.out.println("请输入第"+i+"名员工的年龄：");
            int age=input.nextInt();
            try {
                e.setAge(age);
            }catch (IllegalAgeException err)
            {
                err.printStackTrace();
                break;
            }
            System.out.println("请输入第"+i+"名员工的工资：");
            double salary=input.nextDouble();
            try {
                e.setSalary(salary);
            }catch(IllegalSalaryException err)
            {
                err.printStackTrace();
                break;
            }
            System.out.println("请输入第"+i+"名员工的身份证号码：");
            String identityCardNo=input.next();
            try {
                e.setIdentityCardNo(identityCardNo);
            }catch (IllegalldentityCardNoException err)
            {
                err.printStackTrace();
                break;
            }
        }
        
        for(Employee e:employee)
        {
            e.showEmpInfo();
        }
    }
}
