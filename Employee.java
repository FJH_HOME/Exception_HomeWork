package Exception;

public class Employee {

    private int id;
    private String name;
    private int age;
    private double salary;
    private String identityCardNo;
    public Employee()
    {

    }
    public Employee(int id,String name,int age,double salary,String identityCardNo)throws IllegalSalaryException,IllegalAgeException,IllegalldentityCardNoException
    {
        this.setId(id);
        this.setName(name);
        this.setAge(age);
        this.setSalary(salary);
        this.setIdentityCardNo(identityCardNo);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        if(name==null||name=="")
        {
            throw new NullPointerException("姓名不能为空");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IllegalAgeException
    {
        if(age<18||age>38)
        {
            throw new IllegalAgeException("年龄必须在18~38之间");
        }
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) throws IllegalSalaryException{
        if(salary<0)
        {
            throw new IllegalSalaryException("工资信息错误");
        }else if(salary<3000)
        {
            throw new IllegalSalaryException("员工工资不能低于标准工资");
        }
        this.salary = salary;
    }

    public String getIdentityCardNo() {
        return identityCardNo;
    }

    public void setIdentityCardNo(String identityCardNo) throws IllegalldentityCardNoException{

        if(identityCardNo.length()<18||identityCardNo.length()>18)
        {
            throw new IllegalldentityCardNoException("身份证信息错误");
        }
        this.identityCardNo = identityCardNo;
    }
    public void showEmpInfo()
    {
        System.out.println("工号:"+this.getId());
        System.out.println("姓名:"+this.getName());
        System.out.println("年龄:"+this.getAge());
        System.out.println("工资:"+this.getSalary());
        System.out.println("身份证号码:"+this.getIdentityCardNo());

    }

}
